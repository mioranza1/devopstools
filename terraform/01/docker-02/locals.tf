locals {
  volumes = [
    {
      container_path = "/tmp/dir1"
      host_path      = "/tmp/dir1"
      read_only      = false
    },
    {
      container_path = "/tmp/dir2"
      host_path      = "/tmp/dir2"
      read_only      = true
    },
    {
      container_path = "/var/teste2"
      host_path      = "/tmp/teste2"
      read_only      = true
    },
    {
      container_path = "/var/teste"
      host_path      = "/tmp/teste"
      read_only      = true
    }
  ]

  ports = [
    {
      internal = 80
      external = 8080
    },
    {
      internal = 443
      external = 8443
    }
  ]

  replicas = 4
}