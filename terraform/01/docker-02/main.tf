resource "docker_image" "nginx" {
  name = "nginx:1.19-alpine"
}

resource "docker_container" "nginx-server" {
  count = local.replicas
  name  = "web-${count.index}"
  image = docker_image.nginx.latest

  dynamic "ports" {
    for_each = local.ports
    content {
      internal = ports.value.internal
      external = ports.value.external + count.index
    }
  }

  dynamic "volumes" {
    for_each = local.volumes
    content {
      container_path = volumes.value.container_path
      host_path      = volumes.value.host_path
      read_only      = volumes.value.read_only
    }
  }

}
