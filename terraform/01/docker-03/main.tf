resource "docker_image" "image" {
  name = local.image
}

module "container" {
  source  = "git::https://gitlab.com/mioranza1/terraform-docker-module.git//modules/docker"
  count   = local.replicas
  name    = "target-${count.index}"
  image   = local.image
  volumes = local.volumes
  ports = [
    {
      internal = 80
      external = 8080 + count.index
    },
    {
      internal = 443
      external = 8443 + count.index
    }
  ]
}
