locals {
  volumes = [
    {
      container_path = "/tmp/dir1"
      host_path      = "/tmp/dir1"
      read_only      = false
    },
    {
      container_path = "/tmp/dir2"
      host_path      = "/tmp/dir2"
      read_only      = true
    }
  ]

  replicas = 4
  image    = "nginx:1.19-alpine"
}
