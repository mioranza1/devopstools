# Basic Two-Tier AWS Architecture

## Install awscli
pip3 install awscli

## generate keys and activate ssh-agent
ssh-keygen
eval ssh-agent $SHELL
ssh-add ~/.ssh/id_rsa

## checkov
pip3 install checkov

### testing with checkov
checkov -d .

## conftest
wget https://github.com/open-policy-agent/conftest/releases/download/v0.24.0/conftest_0.24.0_Linux_x86_64.tar.gz
tar xzf conftest_0.24.0_Linux_x86_64.tar.gz
sudo mv conftest /usr/local/bin

### testing with conftest

terraform init
terraform plan -refresh=false -out=plan.tfplan
terraform show -json plan.tfplan >plan.json
conftest pull -p policy/ github.com/fugue/regula/conftest
conftest pull -p policy/regula/lib 'github.com/fugue/regula//lib?ref=v0.7.0'
conftest pull -p policy/regula/rules github.com/fugue/regula/rules
conftest test plan.json

#####################
This provides a template for running a simple two-tier architecture on Amazon
Web services. The premise is that you have stateless app servers running behind
an ELB serving traffic.

To simplify the example, this intentionally ignores deploying and
getting your application onto the servers. However, you could do so either via
[provisioners](https://www.terraform.io/docs/provisioners/) and a configuration
management tool, or by pre-baking configured AMIs with
[Packer](http://www.packer.io).

This example will also create a new EC2 Key Pair in the specified AWS Region. 
The key name and path to the public key must be specified via the
terraform command vars.

After you run `terraform apply` on this configuration, it will
automatically output the DNS address of the ELB. After your instance
registers, this should respond with the default nginx web page.

To run, configure your AWS provider as described in 

https://www.terraform.io/docs/providers/aws/index.html

Run with a command like this:

```
terraform apply -var 'key_name={your_aws_key_name}' \
   -var 'public_key_path={location_of_your_key_in_your_local_machine}'
```

For example:

```
terraform apply -var 'key_name=terraform' -var 'public_key_path=/Users/jsmith/.ssh/terraform.pub'
```

Alternatively to using `-var` with each command, the `terraform.template.tfvars` file can be copied to `terraform.tfvars` and updated.
