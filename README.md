# Repositório usado no treinametno DevOps Tool

## Pre-reqs

1. Vagrant - https://www.vagrantup.com/
2. VirtualBox - https://www.virtualbox.org/

### Somente para máquinas com Windows

1. Git Bash - https://git-scm.com/

## Plugins Vagrant

```
vagrant plugin install vagrant-hostmanager
vagrant plugin install vagrant-disksize
```

## Instruções (Linux/MacOS)

1. Abra o seu terminal
2. Clone este repositório (`git clone https://gitlab.com/mioranza1/devopstools.git`)
3. Entre no diretório criado pelo git clone (devopstools) (`cd devopstools`)
4. Execute `vagrant up`
5. Espere até a mensagem de `done. 😎` apareça.
6. Execute `vagrant ssh` para acessar a máquina virtual criada.
7. Se até aqui tudo está OK você está pronto para as aulas, aproveite!

## Instruções (Windows)

!!! IMPORTANTE !!!

Este lab não foi testatdo com o WSL(1/2), portanto não tente executá-lo através deste recurso.

1. Clique em Iniciar e pesquise por bash, encontre o Git Bash e execute ele
1. Clone este repositório (`git clone https://gitlab.com/mioranza1/devopstools.git`)
2. Entre no diretório criado pelo git clone (devopstools) (`cd devopstools`)
3. Execute `vagrant up`
4. Espere até a mensagem de `done. 😎` apareça.
5. Execute `vagrant ssh` para acessar a máquina virtual criada.
6. Se até aqui tudo está OK você está pronto para as aulas, aproveite!
