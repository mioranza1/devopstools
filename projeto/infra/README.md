# Criação de um cluster EKS e deploy do gitlab-runner para um projeto

Este projeto entrega um cluster EKS na AWS e faz o deploy do gitlab-runner para um projeto que será usado para entrega de uma aplicação.

# Executar esse projeto

1. Acesse o Gitlab.com com suas credenciais
2. Crie um novo projeto chamado **`infra`**, deixe a visibilidade como privada e inicialize o repositório com um arquivo README.md
3. Crie outro projeto com o nome **`app`**, deixe a visibilidade como público e inicialize o repositório com um arquivo README.md
4. Acesse o menu de pereferências do seu usuário e vá na opção **Access Tokens**
5. Crie um novo token com permissão **`api`** e salve o token que será gerado em um lugar seguro, esse token não deve ser exposto
6. Acess o projeto **`infra`** novamente
7. Vá em configurações do projeto na seção CI/CD, **Settings > CICD**
8. Dentro de **CI/CD** acessa a seção **Variables** e crie as variáveis a seguir
    1. **`AWS_ACCESS_KEY_ID`** - credencial AWS
    2. **`AWS_DEFAULT_REGION`** - região onde o cluster será criado
    3. **`AWS_SECRET_ACCESS_KEY`** - credencial AWS
    4. **`MY_PTA`** - Token gerado no passo 4
    5. **`RUNNER_TOKEN`** - Token que deve ser obtido em **Settings > CI/CD > Runners** do projeto **`app`**
9. Clone repositório **`infra`** na sua máquina
10. Copie todo o conteúdo desta pasta **`infra`** para a pasta do novo repositório na sua máquina
11. Execute os comandos em sequencia: **`git add .`** , **`git commit -m "Init"`** e **`git push`**
12. Acompanhe a execução do novo pipeline em **CI/CD > Pipelines** no projeto **`infra`**

O passo de aplicar o cluster de fato é manual, como esse é um procedimento destrutivo ele não será executado automaticamente.

