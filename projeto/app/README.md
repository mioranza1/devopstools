# Aplicação de teste

Este projeto faz parte to projeto de entrega de um cluster EKS e deploy de uma aplicação

> Estes passos devem ser seguidos somente após a criação do cluster seguindo as instruções que constam na pasta **`infra`**

# Executar esse projeto

1. Acesse o Gitlab.com com suas credenciais
2. Crie outro projeto com o nome **`app`**, deixe a visibilidade como público e inicialize o repositório com um arquivo README.md
3. Clone repositório na sua máquina
4. Copie todo o conteúdo desta pasta **`app`** para a pasta do novo repositório na sua máquina
5. Execute os comandos em sequencia: **`git add .`** , **`git commit -m "Init"`** e **`git push`**
6. Acompanhe a execução do novo pipeline em **CI/CD > Pipelines** no projeto **`app`**

# Testar aplicação

1. Obter a URL do serviço de vote através do comando **`kubernetes get svc vote`**
2. Criar um arquivo para simular o voto na opção **`a`** (Cats): **`echo -n "vote=a" > posta`**
3. Criar um arquivo para simular o voto na opção **`b`** (Dogs): **`echo -n "vote=a" > postb`**
4. Gerar requisições para a opção a: `ab -n 1000 -c 50 -p posta -T "application/x-www-form-urlencoded" http://URL_SERVICE:5000/`
5. Gerar requisições para a opção b: `ab -n 1000 -c 50 -p postb -T "application/x-www-form-urlencoded" http://URL_SERVICE:5000/`
6. Acessar aplicação `results` no seu navegador para verificar os votos de cada opção sendo computados, obter a url com o seguinte comando **`kubernetes get svc results`** e acessar via navegador usando `https://URL_RESULTS:5001/`
