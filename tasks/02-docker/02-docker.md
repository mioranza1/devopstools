# Imagem de container

Uma imagem de container ou imagem docker é um arquivo compactado de arquivos compactados.

Quando todos os arquivos são descompactados dentro do mesmo local/diretório então temos o sistema de arquivos do container.

Baixe a imagem de um container: `docker pull redis:3.2.11-alpine`{{execute}}

Salve o container em um arquivo compactado: `docker save redis:3.2.11-alpine > redis.tar`{{execute}}

Agora descompacte o conteúdo do arquivo: `tar -xvf redis.tar`{{execute}}

Verique o conteúdo descompactada: `ls`{{execute}}

Veja as informação sobre as versões e tags: `cat repositories | jq`{{execute}}

`cat manifest.json | jq`{{execute}}

Descompacte uma camada para verificar que arquivos ela contém: `tar -xvf da2a73e79c2ccb87834d7ce3e43d274a750177fe6527ea3f8492d08d3bb0123c/layer.tar`{{execute}}
