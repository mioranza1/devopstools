# Conhecendo o Docker

## Verificando o seu docker

* Verificando a versão do docker: `docker version`{{execute}}
* Mais informações sobre o docker: `docker info`{{execute}}

## O que é um container

`docker run -d --name=db redis:alpine`{{execute}}

Containers são somente processos!

Verificar o pid do seu processo: `ps aux | grep redis-server`{{execute}}

Verificnado o processo pai do seu container: `docker top db`{{execute}}

Copie o valor `PPID` e use no seguinte comando: `ps aux | grep <ppid>` , substitua o <ppid> pelo apresentado no comadno anterior

O comando `pstree` exibe toda a área de processos: `pstree -c -p -A $(pgrep dockerd)`{{execute}}

Nos sistema Unix tudo é um arquivos, no Linux não é diferente, os comandos abaixo irão listar o conteúdo da pasta `/proc` que contém a configuração dos processos que estão rodando, com o ID do processo você pode investigar mais sobre ele. Além diosso eles irão armazenar em uma variável o PID do Redis que foi iniciado via container:

```
DBPID=$(pgrep redis-server)
echo Redis is $DBPID
ls /proc
```{{execute}}

Cada processo possue as suas próprias configurações: `ls /proc/$DBPID`{{execute}}

Por exemplo você pode ver e atualizar as variáveis de ambiente definidas para o processo:

`cat /proc/$DBPID/environ`{{execute}}

ou

`docker exec -it db env`{{execute}}
