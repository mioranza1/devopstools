# Exercícios Git

Configure seu git antes de começar

```
git config --global user.email "devops@targettrust.com.br"
git config --global user.name "DevOps Tools"
echo '[alias]
hist=log -20 --pretty=format:"%C(yellow)%h%Creset\\ %C(green)%ar%C(cyan)%d\\ %Creset%s%C(yellow)\\ [%cn]" --graph --decorate --all' | tee >> ~/.gitconfig
```{{execute}}

Criar um repositório local no diretório git do seu diretório home.

```
cd git
git init
```{{execute}}

Para criar um novo arquivo você pode clicar no nome do arquivo: `git/README.md`{{open}}

O conteúdo do arquivo deve ser igual ao conteúdo abaixo, você pode digitar ou clicar no
botão `Copy to Editor`:

<pre class="file" data-filename="git/README.md" data-target="replace">Este é o arquivo README.md
</pre>

Verifique o status:

`git status`{{execute}}

Adicione o arquivo na área de staging:

`git add README.md`{{execute}}

Verifique o status novamente:

`git status`{{execute}}

Adicione o arquivo no repositório local com uma descrição:

`git commit -m "Versão incial"`{{execute}}

Adicione o conteúdo abaixo no início do arquivo:

<pre class="file" data-filename="git/README.md" data-target="prepend"># Treinamento DevOps Tools TargetTrust

</pre>

Adicione o seu nome no final do arquivo:

<pre class="file" data-filename="git/README.md" data-target="append">
&#x3C;SEU_NOME_AQUI&#x3E;
</pre>

Agora adicone o arquivo na área de staging e então no repositório local com uma nova descrição

Veja o histórico das edições

`git log`{{execute}}
