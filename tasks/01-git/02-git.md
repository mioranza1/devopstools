# Fluxo Centralizado

* Criar projeto com nome centralizado no Gitlab (fizemos via terraform)

## Configurar o git com suas credenciais

```
git config --global user.name "Seu Nome"
git config --global user.email "e-mail@dominio.com.br"
```

Crie o novo comando hist no git:

```
echo '[alias]
hist=log -20 --pretty=format:"%C(yellow)%h%Creset\\ %C(green)%ar%C(cyan)%d\\ %Creset%s%C(yellow)\\ [%cn]" --graph --decorate --all' | tee >> ~/.gitconfig
```{{execute}}

## Criar arquivo README.md para inicializar o seu repositório

```
git clone URL_DO_SEU_REPO
cd centralizado
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
cd ~
```

## Exercício

Criar diretório centralizado dentro da pasta git

`mkdir -p git/centralizado`{{execute}}

Clonar repositório centralizado em dois diretórios diferentes:

```
cd git/centralizado/
git clone URL_DO_SEU_REPO alice
git clone URL_DO_SEU_REPO bob
```

Acessar o diretório alice e verificar o status dos arquivos via git:

`git status`{{execute}}

Criar um arquivo novo chamado alice.txt e inserir o conteúdo abaixo:
Este arquivo foi criado pela Alice, desenvolvedora de sistemas.

`echo "Este arquivo foi criado pela Alice, desenvolvedora de sistemas." > alice.txt`{{execute}}

Criar um segundo arquivo chamado comum.txt e inserir o conteúdo abaixo:
Este arquivo foi criado pela Alice, desenvolvedora de sistemas.

`echo "Este arquivo foi criado pela Alice, desenvolvedora de sistemas." > comum.txt`{{execute}}

Adicionar os arquivos no stage e comitar os arquivos no repositório local com o comentário "Primeira submissão da Alice":

```
git add .
git commit -m "Primeira submissão da Alice"
```{{execute}}

Validar se o commit está OK validando o status do respositório local:

`git status`{{execute}}

`git hist`{{execute}}

Enviar as alterações para o repositório remoto:

`git push origin master`{{execute}}

Acessar projeto via gitlab e confirmar que tudo está de acordo.

Acessar o diretório bob e verificar o status dos arquivos via git:

`git status`{{execute}}

Criar um arquivo novo chamado bob.txt e inserir o conteúdo abaixo:
Este arquivo foi criado pela Bob, desenvolvedor de sistemas.

`echo "Este arquivo foi criado pela Bob, desenvolvedor de sistemas." > bob.txt`{{execute}}

Criar um segundo arquivo chamado comum.txt e inserir o conteúdo abaixo:
Este arquivo foi criado pela Bob, desenvolvedor de sistemas.

`echo "Este arquivo foi criado pela Bob, desenvolvedor de sistemas." > comum.txt`{{execute}}

Adicionar os arquivos no stage e comitar os arquivos no repositório local com o comentário "Primeira submissão do Bob":

```
git add .
git commit -m "Primeira submissão do Bob"
```{{execute}}

Validar se o commit está OK validando o status do respositório local:

`git status`{{execute}}

`git hist`{{execute}}

Enviar as alterações para o repositório remoto:

`git push origin master`{{execute}}

Um erro irá ocorrer devido a não estarmos com o nosso diretório local atualizado em relação ao repositório remoto.
Temos que atualizar a base local com o que temos no repositório remoto mas cuidando para não haver perdas nem sobreposições.
Baixar a versão atual do repositório usando a opção --rebase para organizar os commits e poder resolver os conflitos:

`git pull --rebase origin master`{{execute}}

Novamente temos um erro que nos indica que temos um conflito a ser resolvido no arquivo comum.txt alterado pelos dois desenvolvedores:

```
First, rewinding head to replay your work on top of it...
Applying: Primeira submissão do Bob
Using index info to reconstruct a base tree...
Falling back to patching base and 3-way merge...
Auto-merging comum.txt
CONFLICT (add/add): Merge conflict in comum.txt
error: Failed to merge in the changes.
Patch failed at 0001 Primeira submissão do Bob
Use 'git am --show-current-patch' to see the failed patch

Resolve all conflicts manually, mark them as resolved with
"git add/rm <conflicted_files>", then run "git rebase --continue".
You can instead skip this commit: run "git rebase --skip".
To abort and get back to the state before "git rebase", run "git rebase --abort".
```

Temos que resolver o conflito ou remover o arquivo do nosso commit para poder ter um commit limpo para o repositório remoto.

Acessar projeto via gitlab e confirmar que tudo está de acordo.
O comando abaixo mostar de forma bastante completa o patch que não pode ser aplicado no arquivo:

`git am --show-current-patch`{{execute}}

Para seguirmos adiante iremos alterar o arquivo comum.txt para conter o seguinte conteúdo:

```
echo "Este arquivo foi criado pela Alice, desenvolvedora de sistemas.
Este arquivo foi alterado pelo Bob, desenvolvedor de sistemas." > comum.txt
```{{execute}}

Após esta alteração iremos executar os comandos abaixo para finalizar o rebase:

```
git add .
git rebase --continue
```{{execute}}

Com os conflitos resolvidos visualize o histórico do projeto com o comando abaixo:

`git hist`{{execute}}

Se estiver tudo ok e os commits de Alice e Bob aparecerem na sequência e o comando de status mostra que está tudo OK é hora de enviar para o repositório central:

`git push`{{execute}}

Acessar projeto via gitlab e confirmar que tudo está de acordo.
