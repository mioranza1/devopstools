### Shared Libraries:

**Execute os passos abaixo no repositório do projeto app**

1. Dentro da pasta app criar um diretório chamado vars
2. Dentro do diretório vars criar um arquivo com o nome sharedpipeline.groovy
3. Inserir o conteúdo abaixo no arquivo:

```
def call(Map options = [:]) {
    node {
        cleanWs()

        stage ('Checkout') {
            checkout scm
        }

        stage('Prepare') {
            sh("echo Prepare stage...")
            sh("ls -Rla")
        }

        stage('Build') {
            sh("echo Build stage...")
            sh("hostname")
        }

        stage('Test') {
            sh("echo Test stage...")
            sh("ping -c10 google.com")
        }

        stage('Deploy') {
            sh("echo Deploy stage...")
            sh("[ -e /tmp/teste ] || mkdir /tmp/teste")
        }

    }
}
```

4. No diretório raiz do app copie o arquivo Jenkinsfile para Jenkinsfile.old e crie um novo arquivo Jenkinsfile
5. Insira o conteúdo abaixo no novo Jenkinsfile:

```
@Library('shared-pipeline@master') _
sharedpipeline()
```

6. Envie as alterações para o repositório do Gitlab

**No Jenkins execute os passos abaixo:**

1. Na tela principal do Jenkins selecione `New item`
2. Na tela de novo item insira em nome `shared` e selecione o tipo `Folder`
3. Na configuração do Folder shared vá em `Pipeline Libraries` e insira o nome `shared-pipeline`
4. Em `Retrieval method` selecione `Modern SCM`
5. Em `Source Code Management` selecione `Git`
6. Em `Project Repository` insira a URL de clone do projeto, a URL do seu projeto no Gitlab
7. Em `Credentials` selecione a credencial do seu usuário do Gitlab
8. Salve a configuração do `Folder shared`
9. Dentro do `Folder shared` crie um novo item
10. Na tela de criação do novo item coloque o nome `sharedapp`
11. Selecione o tipo pipeline e salve o novo item
12. Na tela de configuração de `sharedapp` vá em `Pipeline` e selecione `Pipeline script from SCM`
13. Insira os dados do repositório do app tal como fizemos nos passos 6 e 7
14. Em Script path confirme se está somente o nome do arquivo: `Jenkinsfile`
15. Salve a configuração do `sharedapp`.
16. Na tela principal do `sharedapp` clique em `Build now` e veja se a execução ocorre e as etapas do pipeline irão aparecer.

Para simular a criação de um novo projeto iremos criar mais um app que irá usar o pipeline da shared-library:
No Gitlab:
1. Crie um novo projeto chamado `superapp` e inicialize marcando para criar o arquivo README.md
2. Copie a URL de clone do `superapp`

No terminal:
1. Faça o clone do repositório do `superapp`
2. Acesse o diretório do `superapp`
3. Crie um arquivo Jenkinsfile na raíz do repositório do `superapp`
4. Insira o conteúdo abaixo no arquivo Jenkinsfile:

```
@Library('shared-pipeline@master') _
sharedpipeline()
```

5. Salve o arquivo e envie para o repositório do Gitlab.

No Jenkins:
1. Na tela principal do Jenkins acesse o item shared
2. Dentro do Folder shared crie um novo item
3. Na tela de criação do novo item coloque o nome `superapp`
4. Selecione o tipo pipeline e salve o novo item
5. Na tela de configuração de `superapp` vá em Pipeline e selecione Pipeline script from SCM
6. Selecione o tipop Git e insira os dados de clone do repositório do `superapp`
7. Em Script path confirme se está somente o nome do arquivo: Jenkinsfile
8. Salve a configuração do `superapp`.
9. Na tela principal do `superapp` clique em construir agora e veja se a execução ocorre e as etapas do pipeline irão aparecer.