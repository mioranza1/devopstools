## Instalar minikube

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
minikube start --memory=3g --addons=ingress
ln -s $(which minikube) /usr/local/bin/kubectl
kubectl get pods -A
minikube addons enable ingress
```
